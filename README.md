# FoodTransFormation App

## Links

+ [Beginner's guide](https://developer.android.com/training/basics/firstapp/building-ui)
+ [Android room with view model](https://developer.android.com/codelabs/android-room-with-a-view-kotlin#0)
+ [Room persistence library](https://developer.android.com/topic/libraries/architecture/room)
+ [Room queries](https://developer.android.com/training/data-storage/room/accessing-data)
+ [Defining relations between entities](https://developer.android.com/training/data-storage/room/relationships)
+ [Android architechture components](https://github.com/android/architecture-components-samples/tree/master/BasicSample)
+ [Recycler view](https://developer.android.com/guide/topics/ui/layout/recyclerview)
+ [Fragments](https://developer.android.com/guide/fragments/create)
+ [Fragments codelab](https://developer.android.com/codelabs/kotlin-android-training-create-and-add-fragment#4)
+ [Bottom navigation beginner's guide](https://android.jlelse.eu/beginners-guide-to-bottom-navigation-with-android-jetpack-5485d2b8bbb5)
+ [Navigation](https://developer.android.com/guide/navigation/navigation-getting-started)

## Tips

+ When the database schema is updated, **delete** app data from the phone before running the app to prevent migration issues

+ Use the tag ***dev*** to view custom logs in logcat
