package de.ebusiness.ftfapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import de.ebusiness.ftfapp.Const.TAG
import de.ebusiness.ftfapp.LoginActivity.token
import de.ebusiness.ftfapp.fragments.ProfileFragment

class MainActivity : AppCompatActivity() {
     var email: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "MainActivity.onCreate()")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupViews()
        email = intent?.getStringExtra("EMAIL")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_main_logout -> logout();
            R.id.mi_addproduct -> addProduct();

        }
        return true
    }

    private fun addProduct(){
        val intent = Intent(this, AddActivity::class.java)
        startActivity(intent)
    }

    private fun logout() {

        token = null;
        val myIntent = Intent(this, LoginActivity::class.java)
        // set the new task and clear flags
        myIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(myIntent)
    }

    fun setupViews() {
        // Finding the Navigation Controller
        var navController = findNavController(R.id.fragNavHost)
        val bottomNavView = findViewById<BottomNavigationView>(R.id.bottomNavView)

        // Setting Navigation Controller with the BottomNavigationView
        bottomNavView.setupWithNavController(navController)
    }
}