package de.ebusiness.ftfapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class ProductRequest {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("beschreibung")
    @Expose
    private String description;

    @SerializedName("produkt_klasse_id")
    @Expose
    private String productClassId;

    @SerializedName("hersteller_id")
    @Expose
    private String producerId;

    @SerializedName("land_id")
    @Expose
    private String countryId;

    @SerializedName("lebenstil_id")
    @Expose
    private String lifestyleId;

    @SerializedName("transportweg_distanz")
    @Expose
    private String transportDistanceKm;
    
    @SerializedName("zutaten")
    @Expose
    private List<ingredientRequest> ingredients = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProductClassId() {
        return productClassId;
    }

    public void setProductClassId(String productClassId) {
        this.productClassId = productClassId;
    }

    public String getProducerId() {
        return producerId;
    }

    public void setProducerId(String producerId) {
        this.producerId = producerId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getLifestyleId() {
        return lifestyleId;
    }

    public void setLifestyleId(String lifestyleId) {
        this.lifestyleId = lifestyleId;
    }

    public String getTransportDistanceKm() {
        return transportDistanceKm;
    }

    public void setTransportDistanceKm(String transportDistanceKm) {
        this.transportDistanceKm = transportDistanceKm;
    }

    public List<ingredientRequest> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<ingredientRequest> ingredients) {
        this.ingredients = ingredients;
    }

}
