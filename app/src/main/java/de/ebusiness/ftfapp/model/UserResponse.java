package de.ebusiness.ftfapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserResponse {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("encrypted_password")
    @Expose
    private String ePassword;

    public String getName() {
        return name;
    }
}
