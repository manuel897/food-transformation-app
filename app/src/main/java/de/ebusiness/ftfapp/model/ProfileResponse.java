package de.ebusiness.ftfapp.model;

public class ProfileResponse {
    private String email;
    private String newname;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNewName() {
        return newname;
    }

    public void setNewName(String newname) {
        this.newname = newname;
    }
}


