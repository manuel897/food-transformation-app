package de.ebusiness.ftfapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ingredient {

    @SerializedName("zutat_id")
    @Expose
    private Integer ingredientId;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("maßeinheit")
    @Expose
    private String unit;

    @Override
    public String toString() {
            return String.format(name + " ("+ unit + ")");
    }

    public Integer getIngredientId() {
        return ingredientId;
    }
}