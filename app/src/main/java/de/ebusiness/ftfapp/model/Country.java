package de.ebusiness.ftfapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Country {

    @SerializedName("land_id")
    @Expose
    private Integer countryId;

    @SerializedName("name")
    @Expose
    private String name;

    @Override
    public String toString() {
        return String.format(name);
    }


    public Integer getCountryId() {
        return countryId;
    }
}