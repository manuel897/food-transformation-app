package de.ebusiness.ftfapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

public class Product {

    @SerializedName("produkt_id")
    @Expose
    private UUID productId;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("beschreibung")
    @Expose
    private String description;

    @SerializedName("klasse")
    @Expose
    private String productClass;

    @SerializedName("hersteller")
    @Expose
    private String producer;

    @SerializedName("land")
    @Expose
    private String land;

    @SerializedName("lebenstil")
    @Expose
    private String lifestyle;

    // TODO: change to distanz
    @SerializedName("distance")
    @Expose
    private Double transportDistanceKm;

    public Product(String name, String description, String producer, String land, String lifestyle, String productClass, Double transportDistanceKm) {
        this.name = name;
        this.description = description;
        this.producer = producer;
        this.land = land;
        this.lifestyle = lifestyle;
        this.productClass = productClass;
        this.transportDistanceKm = transportDistanceKm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public String getProductClass() {
        return productClass;
    }

    public void setProductClass(String productClass) {
        this.productClass = productClass;
    }

    public String getLand() {
        return land;
    }

    public void setLand(String land) {
        this.land = land;
    }

    public String getLifestyle() {
        return lifestyle;
    }

    public void setLifestyle(String lifestyle) {
        this.lifestyle = lifestyle;
    }

    public Double getTransportDistanceKm() {
        return transportDistanceKm;
    }

    @Override
    public String toString() {
        return String.format(name);
    }

    public UUID getProductId() {
        return productId;
    }

    public String getProducer() {
        return producer;
    }
}