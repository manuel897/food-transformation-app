package de.ebusiness.ftfapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Producer {

    @SerializedName("hersteller_id")
    @Expose
    private Integer producerId;

    @SerializedName("name")
    @Expose
    private String name;

    @Override
    public String toString() {
        return String.format(name);
    }

    public Integer getProducerId() {
        return producerId;
    }
}