package de.ebusiness.ftfapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ingredientRequest {
    @SerializedName("zutat_id")
    @Expose
    private Integer ingredientId;
    @SerializedName("menge")
    @Expose
    private Integer quantity;

    public Integer getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(Integer ingredientId) {
        this.ingredientId = ingredientId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
