package de.ebusiness.ftfapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Lifestyle {

    @SerializedName("lebenstil_id")
    @Expose
    private Integer lifestyleId;

    @SerializedName("name")
    @Expose
    private String name;

    @Override
    public String toString() {
        return String.format(name);
    }


    public Integer getLifestyleId() {
        return lifestyleId;
    }
}