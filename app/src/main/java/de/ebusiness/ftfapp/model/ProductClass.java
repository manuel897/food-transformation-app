package de.ebusiness.ftfapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductClass {

    @SerializedName("produkt_klasse_id")
    @Expose
    private Integer productClassId;

    @SerializedName("name")
    @Expose
    private String name;


    @Override
    public String toString() {
        return String.format(name);
    }

    public int getProductClassId() {
        return productClassId;
    }
}