package de.ebusiness.ftfapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IngredientResponse {


    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("menge")
    @Expose
    private Integer quantity;

    @SerializedName("maßeinheit")
    @Expose
    private String unit;

    @Override
    public String toString() {
        return String.format(name + " ("+ unit + ")");
    }

    public Integer getQuantity() {
        return quantity;
    }
}