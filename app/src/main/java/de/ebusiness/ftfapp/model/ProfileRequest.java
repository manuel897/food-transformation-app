package de.ebusiness.ftfapp.model;

public class ProfileRequest {
        private String email;
        public String newname;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getNewName() {
            return newname;
        }

        public void setnewname(String newname) {
            this.newname = newname;
        }
}

