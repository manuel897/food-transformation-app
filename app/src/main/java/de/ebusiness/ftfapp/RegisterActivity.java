package de.ebusiness.ftfapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import de.ebusiness.ftfapp.Auth.*;
import de.ebusiness.ftfapp.rest.ApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    Button btnSignUp, btnLogin;
    EditText edEmail, edPassword, edName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnSignUp = findViewById(R.id.btnSignUp);
        btnLogin = findViewById(R.id.btnLogin);
        edEmail = findViewById(R.id.edEmail);
        edPassword = findViewById(R.id.edPassword);
        edName = findViewById(R.id.edName);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edEmail.getText().toString()) ||
                        TextUtils.isEmpty(edPassword.getText().toString()) ||
                        TextUtils.isEmpty(edName.getText().toString())) {
                    Toast.makeText(RegisterActivity.this, "Email / Password / User Required",
                            Toast.LENGTH_SHORT).show();
                } else {
                    register();
                }
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }
        });
    }

    public void register() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setEmail(edEmail.getText().toString());
        registerRequest.setPassword(edPassword.getText().toString());
        registerRequest.setName(edName.getText().toString());

        Call<RegisterResponse> registerResponseCall =
                ApiClient.getDataService().userRegister(registerRequest);
        registerResponseCall.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(RegisterActivity.this, "Register Successful!",
                            Toast.LENGTH_SHORT).show();
                    RegisterResponse registerResponse = response.body();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //set fragment after login
                            startActivity(new Intent(RegisterActivity.this, LoginActivity.class).putExtra("data", registerResponse.getEmail()));
                        }
                    }, 700);
                } else {
                    Toast.makeText(RegisterActivity.this, "Register failed!",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, "" + t.getLocalizedMessage(),
                        Toast.LENGTH_SHORT).show();

            }
        });
    }
}