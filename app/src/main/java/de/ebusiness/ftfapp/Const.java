package de.ebusiness.ftfapp;

public final class Const {

    public static final String TAG = "dev";
    public static final String BASE_URL = "https://ftf-db.herokuapp.com";
}
