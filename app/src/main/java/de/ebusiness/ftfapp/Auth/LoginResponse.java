package de.ebusiness.ftfapp.Auth;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("access-token")
    private String accesstoken;

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return accesstoken;
    }

    public void setToken(String token) {
        this.accesstoken = token;
    }
}
