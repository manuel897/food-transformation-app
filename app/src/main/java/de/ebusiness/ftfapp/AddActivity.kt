package de.ebusiness.ftfapp

import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.ebusiness.ftfapp.Const.*
import de.ebusiness.ftfapp.LoginActivity.token
import de.ebusiness.ftfapp.model.*
import de.ebusiness.ftfapp.rest.ApiClient
import de.ebusiness.ftfapp.rest.ErrorResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AddActivity : AppCompatActivity() {
    private val productClasses : ArrayList<ProductClass> = arrayListOf()
    private val producers : ArrayList<Producer> = arrayListOf()
    private val countries : ArrayList<Country> = arrayListOf()
    private val lifestyles : ArrayList<Lifestyle> = arrayListOf()
    private val ingredients : ArrayList<Ingredient> = arrayListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)
        supportActionBar?.setDisplayHomeAsUpEnabled(true);

        // GET request to get all values from backend
        val productClassesCall = ApiClient.getDataService().productClasses
        val producersCall = ApiClient.getDataService().producers
        val countriesCall = ApiClient.getDataService().countries
        val lifestylesCall = ApiClient.getDataService().lifestyles
        val ingredientsCall = ApiClient.getDataService().ingredients

        productClassesCall.enqueue(object : Callback<List<ProductClass>> {
            override fun onResponse(
                call: Call<List<ProductClass>>,
                response: Response<List<ProductClass>>
            ) {
                if (response.code() == 200) {
                    productClasses.addAll(response.body())
                    setClassSpinnerData()
                }
            }

            override fun onFailure(call: Call<List<ProductClass>>, t: Throwable) {
                Log.d(TAG, "response = $t")
            }
        })

        producersCall.enqueue(object : Callback<List<Producer>> {
            override fun onResponse(
                call: Call<List<Producer>>,
                response: Response<List<Producer>>
            ) {
                if (response.code() == 200) {
                    producers.addAll(response.body())
                    setProducerSpinnerData()
                }
            }

            override fun onFailure(call: Call<List<Producer>>, t: Throwable) {
                Log.d(TAG, "response = $t")
            }
        })

        countriesCall.enqueue(object : Callback<List<Country>> {
            override fun onResponse(call: Call<List<Country>>, response: Response<List<Country>>) {
                if (response.code() == 200) {
                    countries.addAll(response.body())
                    setCountrySpinnerData()
                }
            }

            override fun onFailure(call: Call<List<Country>>, t: Throwable) {
                Log.d(TAG, "response = $t")
            }
        })

        lifestylesCall.enqueue(object : Callback<List<Lifestyle>> {
            override fun onResponse(
                call: Call<List<Lifestyle>>,
                response: Response<List<Lifestyle>>
            ) {
                if (response.code() == 200) {
                    lifestyles.addAll(response.body())
                    setLifestyleSpinnerData()
                }
            }

            override fun onFailure(call: Call<List<Lifestyle>>, t: Throwable) {
                Log.d(TAG, "response = $t")
            }
        })

        ingredientsCall.enqueue(object : Callback<List<Ingredient>> {
            override fun onResponse(
                call: Call<List<Ingredient>>,
                response: Response<List<Ingredient>>
            ) {
                if (response.code() == 200) {
                    ingredients.addAll(response.body())
                    setIngredientSpinnerData()
                }
            }

            override fun onFailure(call: Call<List<Ingredient>>, t: Throwable) {
                Log.d(TAG, "response = $t")
            }
        })
    }

    /**
     * Sets the options for product class spinner (dropdown).
     */
    private fun setClassSpinnerData() {
        val classSpinner = findViewById<Spinner>(R.id.class_spinner)

        val productClassAdapter: ArrayAdapter<*> =
            ArrayAdapter<ProductClass>(this, android.R.layout.simple_spinner_item, productClasses)
        classSpinner.adapter = productClassAdapter
    }

    /**
     * Sets the options for producer spinner (dropdown).
     */
    private fun setProducerSpinnerData() {
        val producerSpinner = findViewById<Spinner>(R.id.producer_spinner)

        val producerAdapter: ArrayAdapter<*> =
            ArrayAdapter<Producer>(this, android.R.layout.simple_spinner_item, producers)
        producerSpinner.adapter = producerAdapter
    }

    /**
     * Sets the options for country spinner (dropdown).
     */
    private fun setCountrySpinnerData() {
        val countrySpinner = findViewById<Spinner>(R.id.country_spinner)

        val countryAdapter: ArrayAdapter<*> =
            ArrayAdapter<Country>(this, android.R.layout.simple_spinner_item, countries)
        countrySpinner.adapter = countryAdapter
    }

    /**
     * Sets the options for lifestyle spinner (dropdown).
     */
    private fun setLifestyleSpinnerData() {
        val lifestyleSpinner = findViewById<Spinner>(R.id.lifestyle_spinner)

        val lifestyleAdapter: ArrayAdapter<*> =
            ArrayAdapter<Lifestyle>(this, android.R.layout.simple_spinner_item, lifestyles)
        lifestyleSpinner.adapter = lifestyleAdapter
    }

    /**
     * Sets the options for ingredients spinners (dropdown).
     */
    private fun setIngredientSpinnerData() {
        val ingredientSpinner1 = findViewById<Spinner>(R.id.ingredient1_spinner)
        val ingredientSpinner2 = findViewById<Spinner>(R.id.ingredient2_spinner)

        val ingredientAdapter: ArrayAdapter<*> =
            ArrayAdapter<Ingredient>(this, android.R.layout.simple_spinner_item, ingredients)
        ingredientSpinner1.adapter = ingredientAdapter
        ingredientSpinner2.adapter = ingredientAdapter
    }

    /**
     * sends a POST request to the server to create a product with the user's inputs
     */
    fun onCreateProduct(view: View) {
        Log.d(TAG, "onCreateProduct()")
        val textViewResponse = findViewById<TextView>(R.id.textView_response)
        val progressBar = findViewById<ProgressBar>(R.id.progressBar)
        textViewResponse.visibility = View.GONE

        val nameInput = findViewById<EditText>(R.id.editText_name)
        val descriptionInput = findViewById<EditText>(R.id.editText_description)
        val distanceInput = findViewById<EditText>(R.id.editText_transport_distance)

        // check if inputs are filled
        if(isEmpty(nameInput) || isEmpty(descriptionInput) || isEmpty(distanceInput)) {
            textViewResponse.text = "Bitte alle Felder ausfüllen"
            textViewResponse.visibility = View.VISIBLE
            return
        }

        // getting user inputs
        val selectedCountryPos = findViewById<Spinner>(R.id.country_spinner).selectedItemPosition
        val selectedLifestylePos = findViewById<Spinner>(R.id.lifestyle_spinner).selectedItemPosition
        val selectedProducerPos = findViewById<Spinner>(R.id.producer_spinner).selectedItemPosition
        val selectedProductClassPos = findViewById<Spinner>(R.id.class_spinner).selectedItemPosition
        val selectedIngredient1Pos = findViewById<Spinner>(R.id.ingredient1_spinner).selectedItemPosition
        val selectedIngredient2Pos = findViewById<Spinner>(R.id.ingredient2_spinner).selectedItemPosition
        val selectedIngredients = mutableListOf<ingredientRequest>()

        val req =  ProductRequest()
        req.name = nameInput.text.toString()
        req.description = descriptionInput.text.toString()
        req.transportDistanceKm = distanceInput.text.toString()
        req.countryId = countries[selectedCountryPos].countryId.toString()
        req.lifestyleId = lifestyles[selectedLifestylePos].lifestyleId.toString()
        req.producerId = producers[selectedProducerPos].producerId.toString()
        req.productClassId = productClasses[selectedProductClassPos].productClassId.toString()

        val ingredient1Quantity = findViewById<EditText>(R.id.edit_ingredient1_quantity).text.toString()
        val ingredient2Quantity = findViewById<EditText>(R.id.edit_ingredient2_quantity).text.toString()

        if (ingredient1Quantity != "") {
            val ingredient = ingredientRequest()
            ingredient.ingredientId = ingredients[selectedIngredient1Pos].ingredientId
            ingredient.quantity = Integer.parseInt(ingredient1Quantity)
            selectedIngredients.add(ingredient)
        }
        if (ingredient2Quantity != "") {
            val ingredient = ingredientRequest()
            ingredient.ingredientId = ingredients[selectedIngredient2Pos].ingredientId
            ingredient.quantity = Integer.parseInt(ingredient2Quantity)
            selectedIngredients.add(ingredient)
        }

        req.ingredients = selectedIngredients

        // sending POST request
        val addProductCall = ApiClient.getDataService().addProduct(req, token)

        progressBar.visibility =  View.VISIBLE
        textViewResponse.visibility = View.GONE

        addProductCall.enqueue(object : Callback<ProductRequest> {
            override fun onResponse(
                call: Call<ProductRequest>,
                response: Response<ProductRequest>
            ) {
                if (response.code() == 201) {
                    Log.d(TAG, "response = $response")

                    progressBar.visibility = View.GONE
                    textViewResponse.text = "Produkt erfolgreich hinzugefügt"
                    textViewResponse.visibility = View.VISIBLE
                    openDialog()
                } else if (response.code() == 400) {
                    val gson = Gson()
                    val type = object : TypeToken<ErrorResponse>() {}.type
                    val errorResponse: ErrorResponse? = gson.fromJson(
                        response.errorBody()!!.charStream(), type
                    )

                    progressBar.visibility = View.GONE
                    textViewResponse.text = errorResponse?.message
                    textViewResponse.visibility = View.VISIBLE

                }
            }

            override fun onFailure(call: Call<ProductRequest>, t: Throwable) {
                Log.d(TAG, "response = $t")
                progressBar.visibility = View.GONE
                textViewResponse.visibility = View.VISIBLE
                textViewResponse.text = "Something went wrong"
            }
        })
    }

    /**
     * Returns if the given [etText] is empty.
     */
    private fun isEmpty(etText: EditText): Boolean {
        return etText.text.toString().trim { it <= ' ' }.isEmpty()
    }

    fun openDialog() {
        val builder: AlertDialog.Builder? = this?.let {
            AlertDialog.Builder(it)
        }

        builder?.setMessage(R.string.dialog_product_added)
        //?.setTitle("Added")
        builder?.setNeutralButton(R.string.close,
        DialogInterface.OnClickListener { _, _ ->
            finish()
        })

        val dialog: AlertDialog? = builder?.create()
        dialog?.show()
    }
}