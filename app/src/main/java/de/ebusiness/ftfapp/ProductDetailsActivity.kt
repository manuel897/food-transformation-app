package de.ebusiness.ftfapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.cardview.widget.CardView
import de.ebusiness.ftfapp.Const.*
import de.ebusiness.ftfapp.model.IngredientResponse
import de.ebusiness.ftfapp.rest.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductDetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_details)
        supportActionBar?.setDisplayHomeAsUpEnabled(true);


        findViewById<TextView>(R.id.product_name).text = intent.getStringExtra("PRODUCT_NAME")
        findViewById<TextView>(R.id.product_description).text = intent.getStringExtra("PRODUCT_DESCRIPTION")
        findViewById<TextView>(R.id.product_country).text = intent.getStringExtra("PRODUCT_LAND")
        findViewById<TextView>(R.id.product_lifestyle).text = intent.getStringExtra("PRODUCT_LIFESTYLE")
        findViewById<TextView>(R.id.product_class).text = intent.getStringExtra("PRODUCT_CLASS")
        findViewById<TextView>(R.id.product_distance_km).text = intent.getStringExtra("PRODUCT_DISTANCE")
        findViewById<TextView>(R.id.product_producer).text = intent.getStringExtra("PRODUCT_PRODUCER")




        var ingredients: MutableList<IngredientResponse> = mutableListOf<IngredientResponse>()
        val ingredientsView = findViewById<CardView>(R.id.cardView_ingredients)
        val ingredientsViewTitle = findViewById<TextView>(R.id.textView_ingredients)


        val ingredient1Name = findViewById<TextView>(R.id.zutat1_name)
        val ingredient2Name = findViewById<TextView>(R.id.zutat2_name)
        val ingredient1Quantity = findViewById<TextView>(R.id.zutat1_quantity)
        val ingredient2Quantity = findViewById<TextView>(R.id.zutat2_quantity)
        val productId = intent.getStringExtra("PRODUCT_ID")

        val call = ApiClient.getDataService().findIngredients(productId)

        call.enqueue(object : Callback<List<IngredientResponse>> {
            override fun onResponse(call: Call<List<IngredientResponse>>, response: Response<List<IngredientResponse>>) {
                if (response.code() == 200) {
                    ingredients.addAll(response.body())
                    if (ingredients.isEmpty()) {
                        ingredientsView.visibility = View.GONE
                        ingredientsViewTitle.visibility = View.GONE
                    } else {
                        ingredientsView.visibility = View.VISIBLE
                        ingredientsViewTitle.visibility = View.VISIBLE

                        if (ingredients.count()==1) {
                            ingredient1Name.text = ingredients[0].toString()
                            ingredient1Quantity.text = ingredients[0].quantity.toString()
                            ingredient2Name.text = "-"
                            ingredient2Quantity.text = "-"
                        } else {
                            ingredient1Name.text = ingredients[0].toString()
                            ingredient1Quantity.text = ingredients[0].quantity.toString()
                            ingredient2Name.text = ingredients[1].toString()
                            ingredient2Quantity.text = ingredients[1].quantity.toString()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<List<IngredientResponse>>, t: Throwable) {
                Log.d(TAG, "response = $t")
            }
        })

    }
}
