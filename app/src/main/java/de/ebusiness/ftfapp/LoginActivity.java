package de.ebusiness.ftfapp;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import de.ebusiness.ftfapp.Auth.*;
import de.ebusiness.ftfapp.rest.ApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    public static String token;

    TextInputEditText email, password;
    Button btnLogin, btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.edEmail);
        password = findViewById(R.id.edPassword);
        // name = findViewById(R.id.edname);
        btnLogin = findViewById(R.id.btnLogin);
        btnRegister = findViewById(R.id.btnRegister);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(email.getText().toString()) ||
                        TextUtils.isEmpty(password.getText().toString())) {
                    Toast.makeText(LoginActivity.this, "Email / Password Required",
                            Toast.LENGTH_SHORT).show();
                } else {
                    login();
                }
            }
        });
    }

    public void login() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail(email.getText().toString());
        loginRequest.setPassword(password.getText().toString());

        Call<LoginResponse> loginResponseCall =
                ApiClient.getDataService().userLogin(loginRequest);
        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(LoginActivity.this, "Login Successful!",
                            Toast.LENGTH_SHORT).show();
                    LoginResponse loginResponse = response.body();
                    token = response.headers().get("auth-token");
                    System.out.println("The Token is:"+ token);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // set Fragment after login
                            startActivity(new Intent(LoginActivity.this, MainActivity.class)
                                    .putExtra("EMAIL", loginRequest.getEmail()));
                        }
                    }, 700);
                } else {
                    Toast.makeText(LoginActivity.this, "Login failed!",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "" + t.getLocalizedMessage(),
                        Toast.LENGTH_SHORT).show();

            }
        });
    }
}