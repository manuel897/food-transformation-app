package de.ebusiness.ftfapp.fragments

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.ebusiness.ftfapp.ProductDetailsActivity
import de.ebusiness.ftfapp.R
import de.ebusiness.ftfapp.model.Product

class ProductRecyclerAdapter(private val products: List<Product>) : RecyclerView.Adapter<ProductRecyclerAdapter.ProductHolder>()   {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_result, parent, false)
        return ProductHolder(v)    }

    override fun onBindViewHolder(holder: ProductHolder, position: Int) {
        holder.product = products[position]
        holder.textViewName.text = products[position].name
        holder.textViewDescription.text = products[position].description
    }

    override fun getItemCount() = products.size

    class ProductHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var product: Product? = null
        val textViewName: TextView
        val textViewDescription: TextView

        init {
            itemView.setOnClickListener {
                onClick(itemView)
            }
            textViewName = itemView.findViewById(R.id.product_name)
            textViewDescription = itemView.findViewById(R.id.product_description)
        }

        override fun onClick(v: View) {

            val context = itemView.context
            val showProductIntent = Intent(context, ProductDetailsActivity::class.java)

            // sending info about product to the productDetailsActivity
            showProductIntent.putExtra("PRODUCT_ID",product?.productId.toString())
            showProductIntent.putExtra("PRODUCT_NAME",textViewName.text)
            showProductIntent.putExtra("PRODUCT_DESCRIPTION",product?.description)
            showProductIntent.putExtra("PRODUCT_LAND",product?.land)
            showProductIntent.putExtra("PRODUCT_PRODUCER",product?.producer)
            showProductIntent.putExtra("PRODUCT_LIFESTYLE",product?.lifestyle)
            showProductIntent.putExtra("PRODUCT_CLASS",product?.productClass)
            showProductIntent.putExtra("PRODUCT_DISTANCE",product?.transportDistanceKm.toString())

            context.startActivity(showProductIntent)
        }
    }
}

