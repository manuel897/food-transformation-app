package de.ebusiness.ftfapp.fragments

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import com.google.common.util.concurrent.ListenableFuture
import de.ebusiness.ftfapp.Const.TAG
import de.ebusiness.ftfapp.ProductDetailsActivity
import de.ebusiness.ftfapp.R
import de.ebusiness.ftfapp.model.Product
import de.ebusiness.ftfapp.rest.ApiClient
import java.util.concurrent.ExecutionException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A [Fragment] for scanning for products.
 * Use the [SearchFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ScanFragment : Fragment() {
    private var previewView: PreviewView? = null
    private var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>? = null
    private val PERMISSION_REQUEST_CAMERA = 0
    private var qrCodeFoundButton: Button? = null
    private var qrCode: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_camera, container, false)

        previewView = view.findViewById(R.id.camera_preview);

        cameraProviderFuture = ProcessCameraProvider.getInstance(this.requireContext());
        qrCodeFoundButton = view.findViewById<Button>(R.id.button_product_found)
        qrCodeFoundButton?.visibility = View.INVISIBLE
        qrCodeFoundButton?.setOnClickListener(View.OnClickListener {
            openProductDetailsActivity()
        })
        requestCamera()
        return view
    }

    /**
     * opens product details activity
     */
    private fun openProductDetailsActivity() {
        val call = ApiClient.getDataService().findProductById(qrCode)

        call.enqueue(object : Callback<List<Product>>{
            override fun onResponse(call: Call<List<Product>>, response: Response<List<Product>>) {
                if (response.code() == 200) {
                    val product = response.body()[0]
                    val showProductIntent = Intent(context, ProductDetailsActivity::class.java)

                    // sending info about product to the productDetailsActivity
                    showProductIntent.putExtra("PRODUCT_ID",qrCode)
                    showProductIntent.putExtra("PRODUCT_NAME",product.name)
                    showProductIntent.putExtra("PRODUCT_DESCRIPTION",product.description)
                    showProductIntent.putExtra("PRODUCT_LAND",product.land)
                    showProductIntent.putExtra("PRODUCT_PRODUCER",product.producer)
                    showProductIntent.putExtra("PRODUCT_LIFESTYLE",product.lifestyle)
                    showProductIntent.putExtra("PRODUCT_CLASS",product.productClass)
                    showProductIntent.putExtra("PRODUCT_DISTANCE",product.transportDistanceKm.toString())

                    context?.startActivity(showProductIntent)

                }
            }
            override fun onFailure(call: Call<List<Product>>, t: Throwable) {
                Log.d(TAG, "response = $t")
            }
        })
    }

    /**
     * check if the user has given permission to use camera, if not , ask for permission
     */
    private fun requestCamera() {
        if (ActivityCompat.checkSelfPermission(
                        this.requireActivity(),
                        Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_GRANTED
        ) {
            startCamera()
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                            this.requireActivity(),
                            Manifest.permission.CAMERA
                    )
            ) {
                ActivityCompat.requestPermissions(
                        this.requireActivity(),
                        arrayOf(Manifest.permission.CAMERA),
                        PERMISSION_REQUEST_CAMERA
                )
            } else {
                ActivityCompat.requestPermissions(
                        this.requireActivity(),
                        arrayOf(Manifest.permission.CAMERA),
                        PERMISSION_REQUEST_CAMERA
                )
            }
        }
    }

    /**
     * Callback for the result from requesting permissions. Start camera if the user grants permission
     */
    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String?>,
            grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_REQUEST_CAMERA) {
            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startCamera()
            } else {
                Toast.makeText(
                        this.requireActivity(),
                        "Camera Permission Denied",
                        Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    /**
     * start camera preview
     */
    private fun startCamera() {
        cameraProviderFuture?.addListener({
            try {
                val cameraProvider: ProcessCameraProvider? = cameraProviderFuture?.get()
                if (cameraProvider !== null) {
                    bindCameraPreview(cameraProvider)
                }
            } catch (e: ExecutionException) {
                Toast.makeText(
                        this.requireActivity(), // getting context
                        "Error starting camera " + e.message, Toast.LENGTH_SHORT
                )
                        .show()
            } catch (e: InterruptedException) {
                Toast.makeText(
                        this.requireActivity(),
                        "Error starting camera " + e.message,
                        Toast.LENGTH_SHORT
                )
                        .show()
            }
        }, ContextCompat.getMainExecutor(this.requireActivity()))
    }

    /**
     *  set up and initialize camera preview
     */
    private fun bindCameraPreview(cameraProvider: ProcessCameraProvider) {
        previewView?.preferredImplementationMode = PreviewView.ImplementationMode.SURFACE_VIEW
        val preview = Preview.Builder()
            .build()
        val cameraSelector = CameraSelector.Builder()
            .requireLensFacing(CameraSelector.LENS_FACING_BACK)
            .build()
        preview.setSurfaceProvider(previewView?.createSurfaceProvider())

        preview.setSurfaceProvider(previewView!!.createSurfaceProvider())

        // use custom image analyzer
        val imageAnalysis = ImageAnalysis.Builder()
            .setTargetResolution(Size(1280, 720))
            .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
            .build()

        imageAnalysis.setAnalyzer(
                ContextCompat.getMainExecutor(this.requireActivity()),
                QRCodeImageAnalyzer(object : QRCodeFoundListener {
                    override fun onQRCodeFound(_qrCode: String) {
                        qrCode = _qrCode
                        qrCodeFoundButton?.visibility = View.VISIBLE
                    }

                    override fun qrCodeNotFound() {
                        qrCodeFoundButton?.visibility = View.INVISIBLE
                    }
                })
        )
        val camera = cameraProvider.bindToLifecycle((this as LifecycleOwner), cameraSelector, imageAnalysis, preview)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment.
         *
         * @return A new instance of fragment CameraFragment.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ScanFragment()
    }
}