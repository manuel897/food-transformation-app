package de.ebusiness.ftfapp.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputEditText
import de.ebusiness.ftfapp.Const
import de.ebusiness.ftfapp.Const.TAG
import de.ebusiness.ftfapp.MainActivity
import de.ebusiness.ftfapp.R
import de.ebusiness.ftfapp.model.ProfileRequest
import de.ebusiness.ftfapp.model.ProfileResponse
import de.ebusiness.ftfapp.model.UserResponse
import de.ebusiness.ftfapp.rest.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ProfileFragment : Fragment() {

    val userMail: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val activity: MainActivity? = activity as MainActivity?
        val _email = activity?.email
        loadFullName(_email)
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        val updateButton = view.findViewById<Button>(R.id.btn_update)

        updateButton.setOnClickListener {
            val activity: MainActivity? = activity as MainActivity?
            val _email = activity?.email
            updateName(view, _email)
        }
        return view
    }

    private fun updateName(view: View, _email: String?) {
        val profileRequest = ProfileRequest()

        profileRequest.newname = view.findViewById<TextInputEditText>(R.id.edNewName).text.toString()
        profileRequest.email = _email

        val profileResponseCall = ApiClient.getDataService().updateName(profileRequest)
        profileResponseCall.enqueue(object : Callback<ProfileResponse> {
            override fun onResponse(
                call: Call<ProfileResponse>,
                response: Response<ProfileResponse>
            ) {
                if (response.code() == 200) {
                    val profileResponse: ProfileResponse = response.body()

                    val activity: MainActivity? = activity as MainActivity?
                    val _email = activity?.email

                    loadFullName(_email)
                }
            }

            override fun onFailure(call: Call<ProfileResponse>?, t: Throwable?) {
                Log.d(Const.TAG, "response = $t")
            }

        })

    }

    private fun loadFullName(email: String?) {
        val call = ApiClient.getDataService().findFullName(email)
        Log.d(TAG, "loadFullName($email)")
        call.enqueue(object : Callback<List<UserResponse>> {
            override fun onResponse(
                call: Call<List<UserResponse>>,
                response: Response<List<UserResponse>>
            ) {
                if (response.code() == 200) {
                    val user: UserResponse = response.body()[0]
                    Log.d(TAG, "Response")
                    view?.findViewById<TextView>(R.id.profile_Fullname)?.text = user.name
                }
            }

            override fun onFailure(call: Call<List<UserResponse>>?, t: Throwable?) {
                Log.d(Const.TAG, "response = $t")
            }

        })
    }
}