package de.ebusiness.ftfapp.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.ebusiness.ftfapp.*
import de.ebusiness.ftfapp.Const.TAG
import de.ebusiness.ftfapp.LoginActivity.token
import de.ebusiness.ftfapp.model.Product
import de.ebusiness.ftfapp.rest.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


/**
 * A [Fragment] for searching for products.
 * Use the [SearchFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SearchFragment : Fragment() {

    private var products: ArrayList<Product> = arrayListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_search, container, false)
        val findButton = view.findViewById<Button>(R.id.button_search)
        findButton.setOnClickListener{
            findProduct(view)
        }
        val addButton = view.findViewById<Button>(R.id.button_add)
        addButton.setOnClickListener{
            onAdd()
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // setting up a spinner for sorting
        val sortSpinner: Spinner = view.findViewById(R.id.sort_spinner)
        ArrayAdapter.createFromResource(
            this.requireActivity(), // getting context
            R.array.sort_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            sortSpinner.adapter = adapter
        }

        // setting up listener for the spinner
        sortSpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View, position: Int, id: Long) {
                val recyclerView = view.findViewById<RecyclerView>(R.id.rv_products)

                when(parentView?.getItemAtPosition(position)) {
                    "A - Z" -> {
                        val sortedProducts = products.sortedBy { it.name }
                        products.clear()
                        products.addAll(sortedProducts)
                    }
                    "Z - A" -> {
                        val sortedProducts = products.sortedBy { it.name }.asReversed()
                        products.clear()
                        products.addAll(sortedProducts)
                    }
                    "Nächstgelegene" -> {
                        val sortedProducts = products.sortedBy { it.transportDistanceKm }
                        products.clear()
                        products.addAll(sortedProducts)
                    }
                    "Weiteste" -> {
                        val sortedProducts = products.sortedBy { it.transportDistanceKm }.asReversed()
                        products.clear()
                        products.addAll(sortedProducts)
                    }
                }
                recyclerView.adapter?.notifyDataSetChanged()
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                // TODO: on nothing selected
                Log.d(TAG, "sort nothing selected")
            }
        }
    }

    /**
     * Called when the user taps the search button
     */
    private fun findProduct(view: View) {
        val productsRecyclerView = view.findViewById<RecyclerView>(R.id.rv_products)
        val emptyLayout = view.findViewById<LinearLayout>(R.id.layout_empty)
        val sortSpinner = view.findViewById<Spinner>(R.id.sort_spinner)
        val searchProgressBar = view.findViewById<ProgressBar>(R.id.progressBar_search)

        // setting up view to get response from server
        productsRecyclerView.visibility = View.GONE
        emptyLayout.visibility = View.GONE
        sortSpinner.visibility = View.GONE
        searchProgressBar.visibility = View.VISIBLE

        val searchString = view.findViewById<EditText>(R.id.editText_search).text.toString()

        Log.d(TAG, "findProduct($searchString)")

        // clear products from last search
        products.clear()

        val call = ApiClient.getDataService().findProducts(searchString, token)

        call.enqueue(object : Callback<List<Product>> {
            override fun onResponse(call: Call<List<Product>>, response: Response<List<Product>>) {
                if (response.code() == 200) {
                    searchProgressBar.visibility = View.GONE

                    products.addAll(response.body())

                    if (products.isEmpty()) {
                        Log.d(TAG, "products is empty")
                        productsRecyclerView.visibility = View.GONE
                        emptyLayout.visibility = View.VISIBLE
                        sortSpinner.visibility = View.GONE
                    } else {
                        productsRecyclerView.visibility = View.VISIBLE
                        emptyLayout.visibility = View.GONE
                        sortSpinner.visibility = View.VISIBLE
                    }

                    productsRecyclerView.apply {
                        // set a LinearLayoutManager to handle Android
                        // RecyclerView behavior
                        layoutManager = LinearLayoutManager(activity)
                        // set the custom adapter to the RecyclerView
                        adapter = ProductRecyclerAdapter(products)
                    }
                }
            }

            override fun onFailure(call: Call<List<Product>>, t: Throwable) {
                Log.d(TAG, "response = $t")
                searchProgressBar.visibility = View.GONE
            }
        })
    }

    /**
     * Called when the user taps the add button
     */
    private fun onAdd() {
        Log.d(TAG, "startAddActivity()")
        val intent = Intent(this.requireActivity(), AddActivity::class.java)
        startActivity(intent)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment.
         *
         * @return A new instance of fragment SearchFragment.
         */
        @JvmStatic
        fun newInstance() =
            SearchFragment()
    }
}
