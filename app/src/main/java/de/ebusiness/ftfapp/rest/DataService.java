package de.ebusiness.ftfapp.rest;


import java.util.List;

import de.ebusiness.ftfapp.Auth.*;
import de.ebusiness.ftfapp.model.Country;
import de.ebusiness.ftfapp.model.Ingredient;
import de.ebusiness.ftfapp.model.IngredientResponse;
import de.ebusiness.ftfapp.model.Lifestyle;
import de.ebusiness.ftfapp.model.Producer;
import de.ebusiness.ftfapp.model.Product;
import de.ebusiness.ftfapp.model.ProductClass;
import de.ebusiness.ftfapp.model.ProductRequest;
import de.ebusiness.ftfapp.model.ProfileRequest;
import de.ebusiness.ftfapp.model.ProfileResponse;
import de.ebusiness.ftfapp.model.UserResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface DataService {
    @POST("/api/user/login")
    Call<LoginResponse> userLogin(@Body LoginRequest loginRequest);

    @Headers({"Accept: application/json"})
    @POST("/api/user/register")
    Call<RegisterResponse> userRegister(@Body RegisterRequest registerRequest);

    @GET("products")
    Call<List<Product>> getProducts();

    @GET("products/{name}")
    Call<List<Product>> findProducts(@Path("name") String name,
                                     @Header("auth-token") String token);

    @GET("product/{id}")
    Call<List<Product>> findProductById(@Path("id") String id);

    @GET("ingredients/{id}")
    Call<List<IngredientResponse>> findIngredients(@Path("id") String id);

    @GET("productclasses")
    Call<List<ProductClass>> getProductClasses();

    @GET("producers")
    Call<List<Producer>> getProducers();

    @GET("lifestyles")
    Call<List<Lifestyle>> getLifestyles();

    @GET("countries")
    Call<List<Country>> getCountries();

    @GET("ingredients")
    Call<List<Ingredient>> getIngredients();

    @POST("/product")
        //@FormUrlEncoded
    Call<ProductRequest> addProduct(@Body ProductRequest productRequest,
                                    @Header("auth-token") String token);

    @PUT("/api/user/update")
    Call<ProfileResponse> updateName(@Body ProfileRequest profileRequest);
//                                       @Header("auth-token") String token);

    @GET("user/{email}")
    Call<List<UserResponse>> findFullName(@Path("email") String email);

}
